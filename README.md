# orbitdb-reviewed-access

An OrbitDB Access Controller that grants accessed if an identity has been verified by at least N other reviewed identities.

### Glossary
- OrbitDB: [OrbitDB](orbitdb.org) is a library for CRDT based distributed databases.
- CRDT: Conflict free replicated data types.
- Reviewed Identity: Identity that has been verified by at least MINVERIFICATIONS other reviewed identities.
- Access Controller: A program that checks every OrbitDB Oplog entry before it is added to the Store's oplog.
- MINVERIFICATIONS: Number of verifications necessary for an Identity to be considered reviewed. (This is an AC specific parameter).

